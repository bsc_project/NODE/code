#ifndef __BARBOD_RFM9X__
#define __BARBOD_RFM9X__

#include "stm32f0xx_hal.h"

	
#define BARBOD_RFM9x_SPI_WRITE_MASK 0x80
//not all bit of BARBOD_RFM9x_SPI_WRITE_MASK
#define BARBOD_RFM9x_SPI_READ_MASK 0x7f

// The crystal oscillator frequency of the module
#define BARBOD_RFM9X_FXOSC 32000000.0

#define MAX_PKT_LENGTH           255

// Register names (LoRa Mode, from table 85)
#define BARBOD_RFM9X_REG_00_FIFO                                0x00
#define BARBOD_RFM9X_REG_01_OP_MODE                             0x01
#define BARBOD_RFM9X_REG_02_RESERVED                            0x02
#define BARBOD_RFM9X_REG_03_RESERVED                            0x03
#define BARBOD_RFM9X_REG_04_RESERVED                            0x04
#define BARBOD_RFM9X_REG_05_RESERVED                            0x05
#define BARBOD_RFM9X_REG_06_FRF_MSB                             0x06
#define BARBOD_RFM9X_REG_07_FRF_MID                             0x07
#define BARBOD_RFM9X_REG_08_FRF_LSB                             0x08
#define BARBOD_RFM9X_REG_09_PA_CONFIG                           0x09
#define BARBOD_RFM9X_REG_0A_PA_RAMP                             0x0a
#define BARBOD_RFM9X_REG_0B_OCP                                 0x0b
#define BARBOD_RFM9X_REG_0C_LNA                                 0x0c
#define BARBOD_RFM9X_REG_0D_FIFO_ADDR_PTR                       0x0d
#define BARBOD_RFM9X_REG_0E_FIFO_TX_BASE_ADDR                   0x0e
#define BARBOD_RFM9X_REG_0F_FIFO_RX_BASE_ADDR                   0x0f
#define BARBOD_RFM9X_REG_10_FIFO_RX_CURRENT_ADDR                0x10
#define BARBOD_RFM9X_REG_11_IRQ_FLAGS_MASK                      0x11
#define BARBOD_RFM9X_REG_12_IRQ_FLAGS                           0x12
#define BARBOD_RFM9X_REG_13_RX_NB_BYTES                         0x13
#define BARBOD_RFM9X_REG_14_RX_HEADER_CNT_VALUE_MSB             0x14
#define BARBOD_RFM9X_REG_15_RX_HEADER_CNT_VALUE_LSB             0x15
#define BARBOD_RFM9X_REG_16_RX_PACKET_CNT_VALUE_MSB             0x16
#define BARBOD_RFM9X_REG_17_RX_PACKET_CNT_VALUE_LSB             0x17
#define BARBOD_RFM9X_REG_18_MODEM_STAT                          0x18
#define BARBOD_RFM9X_REG_19_PKT_SNR_VALUE                       0x19
#define BARBOD_RFM9X_REG_1A_PKT_RSSI_VALUE                      0x1a
#define BARBOD_RFM9X_REG_1B_RSSI_VALUE                          0x1b
#define BARBOD_RFM9X_REG_1C_HOP_CHANNEL                         0x1c
#define BARBOD_RFM9X_REG_1D_MODEM_CONFIG1                       0x1d
#define BARBOD_RFM9X_REG_1E_MODEM_CONFIG2                       0x1e
#define BARBOD_RFM9X_REG_1F_SYMB_TIMEOUT_LSB                    0x1f
#define BARBOD_RFM9X_REG_20_PREAMBLE_MSB                        0x20
#define BARBOD_RFM9X_REG_21_PREAMBLE_LSB                        0x21
#define BARBOD_RFM9X_REG_22_PAYLOAD_LENGTH                      0x22
#define BARBOD_RFM9X_REG_23_MAX_PAYLOAD_LENGTH                  0x23
#define BARBOD_RFM9X_REG_24_HOP_PERIOD                          0x24
#define BARBOD_RFM9X_REG_25_FIFO_RX_BYTE_ADDR                   0x25
#define BARBOD_RFM9X_REG_26_MODEM_CONFIG3                       0x26

#define BARBOD_RFM9X_REG_40_DIO_MAPPING1                        0x40
#define BARBOD_RFM9X_REG_41_DIO_MAPPING2                        0x41
#define BARBOD_RFM9X_REG_42_VERSION                             0x42

#define BARBOD_RFM9X_REG_4B_TCXO                                0x4b
#define BARBOD_RFM9X_REG_4D_PA_DAC                              0x4d
#define BARBOD_RFM9X_REG_5B_FORMER_TEMP                         0x5b
#define BARBOD_RFM9X_REG_61_AGC_REF                             0x61
#define BARBOD_RFM9X_REG_62_AGC_THRESH1                         0x62
#define BARBOD_RFM9X_REG_63_AGC_THRESH2                         0x63
#define BARBOD_RFM9X_REG_64_AGC_THRESH3                         0x64

// BARBOD_RFM9X_REG_01_OP_MODE                             0x01
#define BARBOD_RFM9X_LONG_RANGE_MODE                       0x80
#define BARBOD_RFM9X_ACCESS_SHARED_REG                     0x40
#define BARBOD_RFM9X_MODE                                  0x07
#define BARBOD_RFM9X_MODE_SLEEP                            0x00
#define BARBOD_RFM9X_MODE_STDBY                            0x01
#define BARBOD_RFM9X_MODE_FSTX                             0x02
#define BARBOD_RFM9X_MODE_TX                               0x03
#define BARBOD_RFM9X_MODE_FSRX                             0x04
#define BARBOD_RFM9X_MODE_RXCONTINUOUS                     0x05
#define BARBOD_RFM9X_MODE_RXSINGLE                         0x06
#define BARBOD_RFM9X_MODE_CAD                              0x07

// BARBOD_RFM9X_REG_09_PA_CONFIG                           0x09	checked
#define BARBOD_RFM9X_PA_BOOST                              0x80
#define BARBOD_RFM9X_MAX_POWER                             0x70
#define BARBOD_RFM9X_OUTPUT_POWER                          0x0f

// BARBOD_RFM9X_REG_0A_PA_RAMP                             0x0a
#define BARBOD_RFM9X_LOW_PN_TX_PLL_OFF                     0x10
#define BARBOD_RFM9X_PA_RAMP                               0x0f
#define BARBOD_RFM9X_PA_RAMP_3_4MS                         0x00
#define BARBOD_RFM9X_PA_RAMP_2MS                           0x01
#define BARBOD_RFM9X_PA_RAMP_1MS                           0x02
#define BARBOD_RFM9X_PA_RAMP_500US                         0x03
#define BARBOD_RFM9X_PA_RAMP_250US                         0x0
#define BARBOD_RFM9X_PA_RAMP_125US                         0x05
#define BARBOD_RFM9X_PA_RAMP_100US                         0x06
#define BARBOD_RFM9X_PA_RAMP_62US                          0x07
#define BARBOD_RFM9X_PA_RAMP_50US                          0x08
#define BARBOD_RFM9X_PA_RAMP_40US                          0x09
#define BARBOD_RFM9X_PA_RAMP_31US                          0x0a
#define BARBOD_RFM9X_PA_RAMP_25US                          0x0b
#define BARBOD_RFM9X_PA_RAMP_20US                          0x0c
#define BARBOD_RFM9X_PA_RAMP_15US                          0x0d
#define BARBOD_RFM9X_PA_RAMP_12US                          0x0e
#define BARBOD_RFM9X_PA_RAMP_10US                          0x0f

// BARBOD_RFM9X_REG_0B_OCP                                 0x0b
#define BARBOD_RFM9X_OCP_MAX                               0x3f

// BARBOD_RFM9X_REG_0C_LNA                                 0x0c
#define BARBOD_RFM9X_LNA_GAIN                              0xe0
#define BARBOD_RFM9X_LNA_BOOST                             0x03
#define BARBOD_RFM9X_LNA_BOOST_DEFAULT                     0x00
#define BARBOD_RFM9X_LNA_BOOST_150PC                       0x11 //highest gain

// BARBOD_RFM9X_REG_11_IRQ_FLAGS_MASK                      0x11 
#define BARBOD_RFM9X_RX_TIMEOUT_MASK                       0x80
#define BARBOD_RFM9X_RX_DONE_MASK                          0x40
#define BARBOD_RFM9X_PAYLOAD_CRC_ERROR_MASK                0x20
#define BARBOD_RFM9X_VALID_HEADER_MASK                     0x10
#define BARBOD_RFM9X_TX_DONE_MASK                          0x08
#define BARBOD_RFM9X_CAD_DONE_MASK                         0x04
#define BARBOD_RFM9X_FHSS_CHANGE_CHANNEL_MASK              0x02
#define BARBOD_RFM9X_CAD_DETECTED_MASK                     0x01

// BARBOD_RFM9X_REG_12_IRQ_FLAGS                           0x12 
#define BARBOD_RFM9X_RX_TIMEOUT                            0x80
#define BARBOD_RFM9X_RX_DONE                               0x40
#define BARBOD_RFM9X_PAYLOAD_CRC_ERROR                     0x20
#define BARBOD_RFM9X_VALID_HEADER                          0x10
#define BARBOD_RFM9X_TX_DONE                               0x08
#define BARBOD_RFM9X_CAD_DONE                              0x04
#define BARBOD_RFM9X_FHSS_CHANGE_CHANNEL                   0x02
#define BARBOD_RFM9X_CAD_DETECTED                          0x01

// BARBOD_RFM9X_REG_18_MODEM_STAT                          0x18			
#define BARBOD_RFM9X_RX_CODING_RATE                        0xe0
#define BARBOD_RFM9X_MODEM_STATUS_CLEAR                    0x10
#define BARBOD_RFM9X_MODEM_STATUS_HEADER_INFO_VALID        0x08
#define BARBOD_RFM9X_MODEM_STATUS_RX_ONGOING               0x04
#define BARBOD_RFM9X_MODEM_STATUS_SIGNAL_SYNCHRONIZED      0x02
#define BARBOD_RFM9X_MODEM_STATUS_SIGNAL_DETECTED          0x01

// BARBOD_RFM9X_REG_1C_HOP_CHANNEL                         0x1c
#define BARBOD_RFM9X_PLL_TIMEOUT                           0x80
#define BARBOD_RFM9X_RX_PAYLOAD_CRC_IS_ON                  0x40
#define BARBOD_RFM9X_FHSS_PRESENT_CHANNEL                  0x3f

// BARBOD_RFM9X_REG_1D_MODEM_CONFIG1                       0x1d checked
#define BARBOD_RFM9X_CODING_RATE_4_5                       0x02
#define BARBOD_RFM9X_CODING_RATE_4_6                       0x04
#define BARBOD_RFM9X_CODING_RATE_4_7                       0x06
#define BARBOD_RFM9X_CODING_RATE_4_8                       0x08
#define BARBOD_RFM9X_IMPLICIT_HEADER_MODE_ON               0x01
#define BARBOD_RFM9X_IMPLICIT_HEADER_MODE_OFF              0xfe



// BARBOD_RFM9X_REG_1E_MODEM_CONFIG2                       0x1e checked
#define BARBOD_RFM9X_TX_CONTINUOUS_MODE                    0x08
#define BARBOD_RFM9X_CRC_AUTO_ON                           0x04
#define BARBOD_RFM9X_SYM_TIMEOUT_MSB                       0x03

// BARBOD_RFM9X_REG_4D_PA_DAC                              0x4d
#define BARBOD_RFM9X_PA_DAC_DISABLE                        0x84
#define BARBOD_RFM9X_PA_DAC_ENABLE                         0x87




typedef struct BARBOD_rfm9x{
	GPIO_TypeDef* 	NSS_PORT;
	uint16_t				NSS_PIN;
	GPIO_TypeDef* 	RESET_PORT;
	uint16_t				RESET_PIN;
	SPI_HandleTypeDef *hspi;
	//need to add interupt
}BARBOD_rfm9x;
	

void BARBOD_rfm9x_sleep(BARBOD_rfm9x *rfm);
void BARBOD_rfm9x_idle(BARBOD_rfm9x *rfm);
void BARBOD_rfm9x_implicitHeaderMode(BARBOD_rfm9x *rfm);
void BARBOD_rfm9x_explicitHeaderMode(BARBOD_rfm9x *rfm);
void BARBOD_rfm9x_setTxPower(BARBOD_rfm9x *rfm,int level);
void BARBOD_rfm9x_setFrequency(BARBOD_rfm9x *rfm,uint64_t frequency);
void BARBOD_rfm9x_setSpreadingFactor(BARBOD_rfm9x *rfm,int sf);
void BARBOD_rfm9x_setSignalBandwidth(BARBOD_rfm9x *rfm,long sbw);
void BARBOD_rfm9x_setCodingRate4(BARBOD_rfm9x *rfm,int denominator);
void BARBOD_rfm9x_setPreambleLength(BARBOD_rfm9x *rfm,long length);
void BARBOD_rfm9x_crc(BARBOD_rfm9x *rfm);
void BARBOD_rfm9x_noCrc(BARBOD_rfm9x *rfm);
uint8_t BARBOD_rfm9x_begin(BARBOD_rfm9x *rfm,uint8_t channel,uint8_t power);
void BARBOD_rfm9x_end(BARBOD_rfm9x *rfm);
uint8_t BARBOD_rfm9x_beginPacket(BARBOD_rfm9x *rfm,uint8_t implicitHeader);
uint8_t BARBOD_rfm9x_endPacket(BARBOD_rfm9x *rfm);
uint8_t BARBOD_rfm9x_parsePacket(BARBOD_rfm9x *rfm,uint8_t size);
int8_t BARBOD_rfm9x_packetRssi(BARBOD_rfm9x *rfm);
uint8_t BARBOD_readreg(BARBOD_rfm9x *rfm, uint8_t addr);
//float packetSnr();
uint8_t BARBOD_rfm9x_write(BARBOD_rfm9x *rfm,const uint8_t *buffer,uint8_t size);
void BARBOD_rfm9x_receive(BARBOD_rfm9x *rfm,uint8_t size);
uint8_t BARBOD_rfm9x_available(BARBOD_rfm9x *rfm);
uint8_t BARBOD_rfm9x_read(BARBOD_rfm9x *rfm);
uint8_t BARBOD_rfm9x_peek(BARBOD_rfm9x *rfm);
void BARBOD_rfm9x_flush(BARBOD_rfm9x *rfm);
void BARBOD_rfm9x_send(BARBOD_rfm9x *rfm,uint8_t * data,uint8_t retry);
// void onReceive(void(*callback)(int));
//byte random();
//void dumpRegisters(Stream& out);


#endif
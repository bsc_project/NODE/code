#include "barbod_rfm9x.h"
#include <string.h>

uint8_t  _implicitHeaderMode ;
uint64_t _frequency;
uint16_t _packetIndex;


static uint8_t BARBOD_rfm9x_writeregister(BARBOD_rfm9x *rfm,uint8_t name,uint8_t value)
{
	uint8_t status;
	uint8_t temp;
	temp = name | BARBOD_RFM9x_SPI_WRITE_MASK;
	HAL_GPIO_WritePin(rfm->NSS_PORT,rfm->NSS_PIN,GPIO_PIN_RESET);
	HAL_SPI_TransmitReceive(rfm->hspi,&temp,&status,1,10);
	HAL_SPI_Transmit(rfm->hspi,&value,1,10);
	HAL_GPIO_WritePin(rfm->NSS_PORT,rfm->NSS_PIN,GPIO_PIN_SET);
	return status;
}
static uint8_t BARBOD_rfm9x_writeregisters(BARBOD_rfm9x *rfm,uint8_t name,uint8_t *values,uint8_t width)
{
	uint8_t status;
	uint8_t temp;
	temp = name | BARBOD_RFM9x_SPI_WRITE_MASK;
	HAL_GPIO_WritePin(rfm->NSS_PORT,rfm->NSS_PIN,GPIO_PIN_RESET);
	HAL_SPI_TransmitReceive(rfm->hspi,&temp,&status,1,10);
	HAL_SPI_Transmit(rfm->hspi,values,width,10);
	HAL_GPIO_WritePin(rfm->NSS_PORT,rfm->NSS_PIN,GPIO_PIN_SET);
	return status;
}
static uint8_t BARBOD_rfm9x_readregister(BARBOD_rfm9x *rfm,uint8_t name)
{
	uint8_t value;
	uint8_t temp;
	temp = name & BARBOD_RFM9x_SPI_READ_MASK;
  HAL_GPIO_WritePin(rfm->NSS_PORT,rfm->NSS_PIN,GPIO_PIN_RESET);
	HAL_SPI_Transmit(rfm->hspi,&temp,1,10);
	HAL_SPI_Receive(rfm->hspi,&value,1,10);
	HAL_GPIO_WritePin(rfm->NSS_PORT,rfm->NSS_PIN,GPIO_PIN_SET);
	return value;
}

static uint8_t BARBOD_rfm9x_readregisters(BARBOD_rfm9x *rfm,uint8_t name,uint8_t *dest,uint8_t width)
{
	uint8_t status;
	uint8_t temp;
	temp = name & BARBOD_RFM9x_SPI_READ_MASK;
	HAL_GPIO_WritePin(rfm->NSS_PORT,rfm->NSS_PIN,GPIO_PIN_RESET);
	HAL_SPI_TransmitReceive(rfm->hspi,&temp,&status,1,10);
	HAL_SPI_Receive(rfm->hspi,dest,width,10);
	HAL_GPIO_WritePin(rfm->NSS_PORT,rfm->NSS_PIN,GPIO_PIN_SET);
	return status;
}
void BARBOD_rfm9x_sleep(BARBOD_rfm9x *rfm)
{
	BARBOD_rfm9x_writeregister(rfm,BARBOD_RFM9X_REG_01_OP_MODE,BARBOD_RFM9X_MODE_SLEEP|BARBOD_RFM9X_LONG_RANGE_MODE);
}
void BARBOD_rfm9x_idle(BARBOD_rfm9x *rfm)
{
	BARBOD_rfm9x_writeregister(rfm,BARBOD_RFM9X_REG_01_OP_MODE, BARBOD_RFM9X_MODE_STDBY | BARBOD_RFM9X_LONG_RANGE_MODE);
}
void BARBOD_rfm9x_implicitHeaderMode(BARBOD_rfm9x *rfm)
{
	_implicitHeaderMode = 1;
	BARBOD_rfm9x_writeregister(rfm,BARBOD_RFM9X_REG_1D_MODEM_CONFIG1, BARBOD_rfm9x_readregister(rfm,BARBOD_RFM9X_REG_1D_MODEM_CONFIG1) | BARBOD_RFM9X_IMPLICIT_HEADER_MODE_ON);
}
void BARBOD_rfm9x_explicitHeaderMode(BARBOD_rfm9x *rfm)
{
	_implicitHeaderMode = 0;
	BARBOD_rfm9x_writeregister(rfm,BARBOD_RFM9X_REG_1D_MODEM_CONFIG1, BARBOD_rfm9x_readregister(rfm,BARBOD_RFM9X_REG_1D_MODEM_CONFIG1) & BARBOD_RFM9X_IMPLICIT_HEADER_MODE_OFF);
}
void BARBOD_rfm9x_setTxPower(BARBOD_rfm9x *rfm,int level)
{
	if (level < 2) {
    level = 2;
  } else if (level > 17) {
    level = 17;
  }
	BARBOD_rfm9x_writeregister(rfm,BARBOD_RFM9X_REG_0B_OCP,BARBOD_RFM9X_OCP_MAX);
	BARBOD_rfm9x_writeregister(rfm,BARBOD_RFM9X_REG_4D_PA_DAC,BARBOD_RFM9X_PA_DAC_ENABLE);
  BARBOD_rfm9x_writeregister(rfm,BARBOD_RFM9X_REG_09_PA_CONFIG, BARBOD_RFM9X_PA_BOOST | (level - 2));
	
}
void BARBOD_rfm9x_setFrequency(BARBOD_rfm9x *rfm,uint64_t frequency)
{
	_frequency = frequency;

  uint64_t frf = ((uint64_t)frequency << 19) / 32000000;
  BARBOD_rfm9x_writeregister(rfm,BARBOD_RFM9X_REG_06_FRF_MSB, (uint8_t)(frf >> 16));
  BARBOD_rfm9x_writeregister(rfm,BARBOD_RFM9X_REG_07_FRF_MID, (uint8_t)(frf >> 8));
  BARBOD_rfm9x_writeregister(rfm,BARBOD_RFM9X_REG_08_FRF_LSB, (uint8_t)(frf >> 0));
}

void BARBOD_rfm9x_setSpreadingFactor(BARBOD_rfm9x *rfm,int sf)
{
	if (sf < 6) {
    sf = 6;
  } else if (sf > 12) {
    sf = 12;
  }

  BARBOD_rfm9x_writeregister(rfm,BARBOD_RFM9X_REG_1E_MODEM_CONFIG2, (BARBOD_rfm9x_readregister(rfm,BARBOD_RFM9X_REG_1E_MODEM_CONFIG2) & 0x0f) | ((sf << 4) & 0xf0));	
}
void BARBOD_rfm9x_setSignalBandwidth(BARBOD_rfm9x *rfm,long sbw)
{
	int bw;
  if (sbw <= 7.8E3) bw = 0;
  else if (sbw <= 10.4E3) bw = 1;
  else if (sbw <= 15.6E3) bw = 2;
  else if (sbw <= 20.8E3) bw = 3;
  else if (sbw <= 31.25E3)bw = 4;
  else if (sbw <= 41.7E3) bw = 5;
  else if (sbw <= 62.5E3) bw = 6;
  else if (sbw <= 125E3)  bw = 7;
  else if (sbw <= 250E3)  bw = 8;
  else bw = 9;
	BARBOD_rfm9x_writeregister(rfm,BARBOD_RFM9X_REG_1D_MODEM_CONFIG1, (BARBOD_rfm9x_readregister(rfm,BARBOD_RFM9X_REG_1D_MODEM_CONFIG1) & 0x0f) | (bw << 4));
}
void BARBOD_rfm9x_setCodingRate4(BARBOD_rfm9x *rfm,int denominator)
{
	if (denominator < 5) {
    denominator = 5;
  } else if (denominator > 8) {
    denominator = 8;
  }

  int cr = denominator - 4;

  BARBOD_rfm9x_writeregister(rfm,BARBOD_RFM9X_REG_1D_MODEM_CONFIG1, (BARBOD_rfm9x_readregister(rfm,BARBOD_RFM9X_REG_1D_MODEM_CONFIG1) & 0xf1) | (cr << 1));
}
void BARBOD_rfm9x_setPreambleLength(BARBOD_rfm9x *rfm,long length)
{
	BARBOD_rfm9x_writeregister(rfm,BARBOD_RFM9X_REG_20_PREAMBLE_MSB, (uint8_t)(length >> 8));
	BARBOD_rfm9x_writeregister(rfm,BARBOD_RFM9X_REG_21_PREAMBLE_LSB, (uint8_t)(length >> 0));
}
void BARBOD_rfm9x_crc(BARBOD_rfm9x *rfm)
{
	BARBOD_rfm9x_writeregister(rfm,BARBOD_RFM9X_REG_1E_MODEM_CONFIG2, BARBOD_rfm9x_readregister(rfm,BARBOD_RFM9X_REG_1E_MODEM_CONFIG2) | 0x04);
	BARBOD_rfm9x_writeregister(rfm,BARBOD_RFM9X_REG_1C_HOP_CHANNEL,BARBOD_rfm9x_readregister(rfm,BARBOD_RFM9X_REG_1C_HOP_CHANNEL) | 0x40);
}
void BARBOD_rfm9x_noCrc(BARBOD_rfm9x *rfm)
{
	BARBOD_rfm9x_writeregister(rfm,BARBOD_RFM9X_REG_1E_MODEM_CONFIG2, BARBOD_rfm9x_readregister(rfm,BARBOD_RFM9X_REG_1E_MODEM_CONFIG2) & 0xfb);
}
uint8_t BARBOD_rfm9x_begin(BARBOD_rfm9x *rfm,uint8_t channel,uint8_t power)
{
	  // perform reset
  HAL_GPIO_WritePin(rfm->RESET_PORT,rfm->RESET_PIN, GPIO_PIN_RESET);
	HAL_Delay(10);
  HAL_GPIO_WritePin(rfm->RESET_PORT,rfm->RESET_PIN, GPIO_PIN_SET);
	HAL_Delay(10);
  HAL_GPIO_WritePin(rfm->NSS_PORT,rfm->NSS_PIN, GPIO_PIN_SET);
	BARBOD_rfm9x_sleep(rfm);
	BARBOD_rfm9x_setFrequency(rfm,(long)(433E6 + channel*1.5E6));
	BARBOD_rfm9x_writeregister(rfm,BARBOD_RFM9X_REG_0E_FIFO_TX_BASE_ADDR,0);
	BARBOD_rfm9x_writeregister(rfm,BARBOD_RFM9X_REG_0F_FIFO_RX_BASE_ADDR,0);
	BARBOD_rfm9x_writeregister(rfm,BARBOD_RFM9X_REG_0C_LNA,BARBOD_rfm9x_readregister(rfm,BARBOD_RFM9X_REG_0C_LNA) | 0x03 );
//	BARBOD_rfm9x_writeregister(rfm,BARBOD_RFM9X_REG_11_IRQ_FLAGS_MASK,BARBOD_RFM9X_CAD_DETECTED_MASK
//																																	| BARBOD_RFM9X_FHSS_CHANGE_CHANNEL_MASK
//																																	| BARBOD_RFM9X_CAD_DONE_MASK
//																																	| BARBOD_RFM9X_VALID_HEADER_MASK
//																																	| BARBOD_RFM9X_RX_TIMEOUT_MASK);
																										
  BARBOD_rfm9x_crc(rfm);					
	BARBOD_rfm9x_setPreambleLength(rfm,10);
	BARBOD_rfm9x_setSignalBandwidth(rfm,500E3);
  BARBOD_rfm9x_setSpreadingFactor(rfm,6);
	BARBOD_rfm9x_setTxPower(rfm,power);
	BARBOD_rfm9x_idle(rfm);
	
	return 1;
}
void BARBOD_rfm9x_end(BARBOD_rfm9x *rfm)
{
	BARBOD_rfm9x_sleep(rfm);
}
uint8_t BARBOD_rfm9x_beginPacket(BARBOD_rfm9x *rfm,uint8_t implicitHeader)
{
	  // put in standby mode
  BARBOD_rfm9x_idle(rfm);
  if (implicitHeader) BARBOD_rfm9x_implicitHeaderMode(rfm);
   else BARBOD_rfm9x_explicitHeaderMode(rfm);
  
  // reset FIFO address and payload length
  BARBOD_rfm9x_writeregister(rfm,BARBOD_RFM9X_REG_0D_FIFO_ADDR_PTR, 0);
  BARBOD_rfm9x_writeregister(rfm,BARBOD_RFM9X_REG_22_PAYLOAD_LENGTH, 0);

  return 1;
}
uint8_t BARBOD_rfm9x_endPacket(BARBOD_rfm9x *rfm)
{
	// put in TX mode
  BARBOD_rfm9x_writeregister(rfm,BARBOD_RFM9X_REG_01_OP_MODE, BARBOD_RFM9X_LONG_RANGE_MODE | BARBOD_RFM9X_MODE_TX);

  // wait for TX done
  while((BARBOD_rfm9x_readregister(rfm,BARBOD_RFM9X_REG_12_IRQ_FLAGS) & BARBOD_RFM9X_TX_DONE) != BARBOD_RFM9X_TX_DONE);
	

  // clear IRQ's
  BARBOD_rfm9x_writeregister(rfm,BARBOD_RFM9X_REG_12_IRQ_FLAGS, BARBOD_RFM9X_TX_DONE);

  return 1;
}
uint8_t BARBOD_rfm9x_parsePacket(BARBOD_rfm9x *rfm,uint8_t size)
{
	
	uint8_t packetLength = 0;
  uint8_t irqFlags = BARBOD_rfm9x_readregister(rfm,BARBOD_RFM9X_REG_12_IRQ_FLAGS);

  if (size > 0) {
    BARBOD_rfm9x_implicitHeaderMode(rfm);
    BARBOD_rfm9x_writeregister(rfm,BARBOD_RFM9X_REG_22_PAYLOAD_LENGTH, size & 0xff);
  } else {
    BARBOD_rfm9x_explicitHeaderMode(rfm);
  }

  // clear IRQ's
  BARBOD_rfm9x_writeregister(rfm,BARBOD_RFM9X_REG_12_IRQ_FLAGS, irqFlags);

  if ((irqFlags & BARBOD_RFM9X_RX_DONE) && (irqFlags & BARBOD_RFM9X_PAYLOAD_CRC_ERROR_MASK) == 0) {
    // received a packet
    _packetIndex = 0;

    // read packet length
    if (_implicitHeaderMode) {
      packetLength = BARBOD_rfm9x_readregister(rfm,BARBOD_RFM9X_REG_22_PAYLOAD_LENGTH);
    } else {
      packetLength = BARBOD_rfm9x_readregister(rfm,BARBOD_RFM9X_REG_13_RX_NB_BYTES);
    }

    // set FIFO address to current RX address
    BARBOD_rfm9x_writeregister(rfm,BARBOD_RFM9X_REG_0D_FIFO_ADDR_PTR, BARBOD_rfm9x_readregister(rfm,BARBOD_RFM9X_REG_10_FIFO_RX_CURRENT_ADDR));

    // put in standby mode
    BARBOD_rfm9x_idle(rfm);
  } else if (BARBOD_rfm9x_readregister(rfm,BARBOD_RFM9X_REG_01_OP_MODE) != (BARBOD_RFM9X_LONG_RANGE_MODE | BARBOD_RFM9X_MODE_RXSINGLE)) {
    // not currently in RX mode

    // reset FIFO address
    BARBOD_rfm9x_writeregister(rfm,BARBOD_RFM9X_REG_0D_FIFO_ADDR_PTR, 0);

    // put in single RX mode
    BARBOD_rfm9x_writeregister(rfm,BARBOD_RFM9X_REG_01_OP_MODE, BARBOD_RFM9X_LONG_RANGE_MODE | BARBOD_RFM9X_MODE_RXSINGLE);
		
  }

  return packetLength;
}
int8_t BARBOD_rfm9x_packetRssi(BARBOD_rfm9x *rfm)
{
	return (BARBOD_rfm9x_readregister(rfm,BARBOD_RFM9X_REG_1A_PKT_RSSI_VALUE) - 137);
}
uint8_t BARBOD_readreg(BARBOD_rfm9x *rfm, uint8_t add)
{
	return BARBOD_rfm9x_readregister(rfm, add);
}
uint8_t BARBOD_rfm9x_write(BARBOD_rfm9x *rfm,const uint8_t *buffer,uint8_t size)
{
	  uint8_t currentLength = BARBOD_rfm9x_readregister(rfm,BARBOD_RFM9X_REG_22_PAYLOAD_LENGTH);
		
		// check size
  if ((currentLength + size) > MAX_PKT_LENGTH) {
    size = MAX_PKT_LENGTH - currentLength;
  }

  // write data
  for (size_t i = 0; i < size; i++) {
    BARBOD_rfm9x_writeregister(rfm,BARBOD_RFM9X_REG_00_FIFO, buffer[i]);
  }

  // update length
  BARBOD_rfm9x_writeregister(rfm,BARBOD_RFM9X_REG_22_PAYLOAD_LENGTH, currentLength + size);

  return size;
}
void BARBOD_rfm9x_receive(BARBOD_rfm9x *rfm,uint8_t size)
{
	
  if (size > 0) {
    BARBOD_rfm9x_implicitHeaderMode(rfm);
    BARBOD_rfm9x_writeregister(rfm,BARBOD_RFM9X_REG_22_PAYLOAD_LENGTH, size & 0xff);
  } else {
    BARBOD_rfm9x_explicitHeaderMode(rfm);
  }
  BARBOD_rfm9x_writeregister(rfm,BARBOD_RFM9X_REG_01_OP_MODE, BARBOD_RFM9X_LONG_RANGE_MODE | BARBOD_RFM9X_MODE_RXCONTINUOUS);
}

uint8_t BARBOD_rfm9x_available(BARBOD_rfm9x *rfm)
{
	return (BARBOD_rfm9x_readregister(rfm,BARBOD_RFM9X_REG_13_RX_NB_BYTES) - _packetIndex);
}
uint8_t BARBOD_rfm9x_read(BARBOD_rfm9x *rfm)
{
	if (!BARBOD_rfm9x_available(rfm)) {
    return -1;
  }

  _packetIndex++;
  return BARBOD_rfm9x_readregister(rfm,BARBOD_RFM9X_REG_00_FIFO);
}
uint8_t BARBOD_rfm9x_peek(BARBOD_rfm9x *rfm)
{
	  if (!BARBOD_rfm9x_available(rfm)) {
    return -1;
  }


  // store current FIFO address
  int currentAddress = BARBOD_rfm9x_readregister(rfm,BARBOD_RFM9X_REG_0D_FIFO_ADDR_PTR);

  // read
  uint8_t b = BARBOD_rfm9x_readregister(rfm,BARBOD_RFM9X_REG_00_FIFO);

  // restore FIFO address
  BARBOD_rfm9x_writeregister(rfm,BARBOD_RFM9X_REG_0D_FIFO_ADDR_PTR, currentAddress);

  return b;
}
void BARBOD_rfm9x_flush(BARBOD_rfm9x *rfm)
{
	
}
void BARBOD_rfm9x_send(BARBOD_rfm9x *rfm,uint8_t * data,uint8_t retry)
{
	uint8_t j=0;
				for(j = 0 ; j<retry;j++)
			{
				BARBOD_rfm9x_beginPacket(rfm,1);
				BARBOD_rfm9x_write(rfm,data,8);
				BARBOD_rfm9x_endPacket(rfm);
			}
}